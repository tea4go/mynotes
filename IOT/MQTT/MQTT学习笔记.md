
# MQTT学习笔记
* 作者：[刘启铨](LiuQiQuan@gmail.com)
* 日期：2017-10-26  

## MQTT介绍
MQTT是IBM开发的一个即时通讯协议。MQTT是面向M2M和物联网的连接协议，采用轻量级发布和订阅消息传输机制。Mosquitto是一款实现了 MQTT v3.1 协议的开源消息代理软件，提供轻量级的，支持发布/订阅的的消息推送模式，使设备对设备之间的短消息通信简单易用。

若初次接触MQTT协议，可先理解以下概念:
* 【MQTT协议特点】——相比于RESTful架构的物联网系统，MQTT协议借助消息推送功能，可以`更好地实现远程控制`。
* 【MQTT协议角色】——在RESTful架构的物联网系统，包含两个角色客户端和服务器端，而在MQTT协议中包括`发布者，代理器（服务器）和订阅者`。
* 【MQTT协议消息】——MQTT中的消息可理解为发布者和订阅者交换的内容（负载），这些消息包含具体的内容，可以被订阅者使用。
* 【MQTT协议主题】——MQTT中的主题可理解为相同类型或相似类型的消息集合。

## 安装  

* **下载源码**  
      wget http://mosquitto.org/files/source/mosquitto-1.4.14.tar.gz    
* **解压**  
      tar zxfv mosquitto-1.4.5.tar.gz
* **编译&安装**    
      make
      make install
## 配置参数
安装完成之后，所有配置文件会被放置于/etc/mosquitto/目录下，其中最重要的就是Mosquitto的配置文件，即mosquitto.conf，以下是详细的配置参数说明。

    # =================================================================
    # General configuration
    # =================================================================

    # 客户端心跳的间隔时间
    #retry_interval 20

    # 系统状态的刷新时间
    #sys_interval 10

    # 系统资源的回收时间，0表示尽快处理
    #store_clean_interval 10

    # 服务进程的PID
    #pid_file /var/run/mosquitto.pid

    # 服务进程的系统用户
    #user mosquitto

    # 客户端心跳消息的最大并发数
    #max_inflight_messages 10

    # 客户端心跳消息缓存队列
    #max_queued_messages 100

    # 用于设置客户端长连接的过期时间，默认永不过期
    #persistent_client_expiration

    # =================================================================
    # Default listener
    # =================================================================

    # 服务绑定的IP地址
    #bind_address

    # 服务绑定的端口号
    #port 1883

    # 允许的最大连接数，-1表示没有限制
    #max_connections -1

    # cafile：CA证书文件
    # capath：CA证书目录
    # certfile：PEM证书文件
    # keyfile：PEM密钥文件
    #cafile
    #capath
    #certfile
    #keyfile

    # 必须提供证书以保证数据安全性
    #require_certificate false

    # 若require_certificate值为true，use_identity_as_username也必须为true
    #use_identity_as_username false

    # 启用PSK（Pre-shared-key）支持
    #psk_hint

    # SSL/TSL加密算法，可以使用“openssl ciphers”命令获取
    # as the output of that command.
    #ciphers

    # =================================================================
    # Persistence
    # =================================================================

    # 消息自动保存的间隔时间
    #autosave_interval 1800

    # 消息自动保存功能的开关
    #autosave_on_changes false

    # 持久化功能的开关
    persistence true

    # 持久化DB文件
    #persistence_file mosquitto.db

    # 持久化DB文件目录
    #persistence_location /var/lib/mosquitto/

    # =================================================================
    # Logging
    # =================================================================

    # 4种日志模式：stdout、stderr、syslog、topic
    # none 则表示不记日志，此配置可以提升些许性能
    log_dest none

    # 选择日志的级别（可设置多项）
    #log_type error
    #log_type warning
    #log_type notice
    #log_type information

    # 是否记录客户端连接信息
    #connection_messages true

    # 是否记录日志时间
    #log_timestamp true

    # =================================================================
    # Security
    # =================================================================

    # 客户端ID的前缀限制，可用于保证安全性
    #clientid_prefixes

    # 允许匿名用户
    #allow_anonymous true

    # 用户/密码文件，默认格式：username:password
    #password_file

    # PSK格式密码文件，默认格式：identity:key
    #psk_file

    # pattern write sensor/%u/data
    # ACL权限配置，常用语法如下：
    # 用户限制：user <username>
    # 话题限制：topic [read|write] <topic>
    # 正则限制：pattern write sensor/%u/data
    #acl_file

    # =================================================================
    # Bridges
    # =================================================================

    # 允许服务之间使用“桥接”模式（可用于分布式部署）
    #connection <name>
    #address <host>[:<port>]
    #topic <topic> [[[out | in | both] qos-level] local-prefix remote-prefix]

    # 设置桥接的客户端ID
    #clientid

    # 桥接断开时，是否清除远程服务器中的消息
    #cleansession false

    # 是否发布桥接的状态信息
    #notifications true

    # 设置桥接模式下，消息将会发布到的话题地址
    # $SYS/broker/connection/<clientid>/state
    #notification_topic

    # 设置桥接的keepalive数值
    #keepalive_interval 60

    # 桥接模式，目前有三种：automatic、lazy、once
    #start_type automatic

    # 桥接模式automatic的超时时间
    #restart_timeout 30

    # 桥接模式lazy的超时时间
    #idle_timeout 60

    # 桥接客户端的用户名
    #username

    # 桥接客户端的密码
    #password

    # bridge_cafile：桥接客户端的CA证书文件
    # bridge_capath：桥接客户端的CA证书目录
    # bridge_certfile：桥接客户端的PEM证书文件
    # bridge_keyfile：桥接客户端的PEM密钥文件
    #bridge_cafile
    #bridge_capath
    #bridge_certfile
    #bridge_keyfile

    # 自己的配置可以放到以下目录中
    include_dir /etc/mosquitto/conf.d

## 编译参数
    # 是否支持tcpd/libwrap功能.  
    #WITH_WRAP:=yes  
      
    # 是否开启SSL/TLS支持  
    #WITH_TLS:=yes  
      
    # 是否开启TLS/PSK支持  
    #WITH_TLS_PSK:=yes  
      
    # Comment out to disable client client threading support.  
    #WITH_THREADING:=yes  
      
    # 是否使用严格的协议版本（老版本兼容会有点问题）  
    #WITH_STRICT_PROTOCOL:=yes  
      
    # 是否开启桥接模式  
    #WITH_BRIDGE:=yes  
      
    # 是否开启持久化功能  
    #WITH_PERSISTENCE:=yes  
      
    # 是否监控运行状态  
    #WITH_MEMORY_TRACKING:=yes  

    #启用c-areas库的支持，一个支持异步DNS查找的库，详见http://c-ares.haxx.se  
    #WITH_SRV:=yes  

    # 启用lib-uuid支持，支持为每个连接的客户端生成唯一的uuid  
    #WITH_UUID:=yes  
      
    # 启用websocket支持，需安装libwebsockets对于需要使用websocket协议的应用开启  
    #WITH_WEBSOCKETS:=yes

## 简单测试
一个完整的MQTT示例包括一个代理器，一个发布者和一个订阅者。测试分为以下几个步骤：
1. 启动服务mosquitto。
2. 订阅者通过mosquitto_sub订阅指定主题的消息。
3. 发布者通过mosquitto_pub发布指定主题的消息。
4. 代理服务器把该主题的消息推送到订阅者。

【测试说明】\
    测试环境：ubuntu 14.04 虚拟机\
    在本例中，发布者、代理和订阅者均为localhsot，但是在实际的情况下三种并不是同一个设备，在mosquitto中可通过-h(--host)设置主机名称(hostname)。为了实现这个简单的测试案例，需要在linux中打开三个控制台，分别代表代理服务器、发布者和订阅者。
* 启动代理服务
```
   mosquitto -v -c /etc/mosquitto/mosquitto.conf -d
   参数：
    【-c】指定配置文件运行
    【-d】以服务的方式执行
    【-v】打印更多的调试信息
```
> 注：调整系统的最大连接数和栈大小\
>     ulimit -n20000 -s512
     
* 订阅主题
```
   mosquitto_sub -v -t sensor
   参数：
    【-t】指定主题，此处为sensor
    【-v】打印更多的调试信息
```

* 发布内容
```
   mosquitto_pub -t sensor  -m 12
   参数：
    【-t】指定主题
    【-m】指定消息内容
```

## FAQ
  1. 编译找不到openssl/ssl.h  
```
   #安装openssl    
   apt-get install libssl-dev
```
  2. 编译过程找不到ares.h
```
   sudo apt-get install libc-ares-dev
```  
  3. 编译过程找不到uuid/uuid.h
```   
   sudo apt-get install uuid-dev
```  
  4. 编译过程找不到websocket
  >mosquitto.c:47:29: fatal error: libwebsockets.h: 没有那个文件或目录\
  >#include <libwebsockets.h> 
```   
	git clone https://github.com/hfeeki/libwebsockets
	cd libwebsockets
	mkdir build 
    cd build
    cmake .. -DLIB_SUFFIX=64 -DLWS_WITH_HTTP2=1
    make install 
``` 
  >如果遇到编译找不到libz.so，libssl.so，libcrypto.so库，或找错了库版本不对，则需要在$PATH把正确的库路径放在PATH的前面。

  3. 编译过程找不到uuid/uuid.h
```   
   sudo apt-get install uuid-dev
``` 
  3. 编译过程找不到uuid/uuid.h
```   
   sudo apt-get install uuid-dev
``` 
  4. 使用过程中找不到libmosquitto.so.1  
> error while loading shared libraries: libmosquitto.so.1: cannot open shared object file: No such file or directory
```      
   #修改libmosquitto.so位置    
   ln -s /usr/local/lib/libmosquitto.so.1 /usr/lib/libmosquitto.so.1    
   ldconfig
```
  5. make: g++：命令未找到  
```         
    #安装g++编译器  
    sudo apt-get install g++
```  
  6. Invalid user 'mosquitto'.  
>[root@localhost src]# ./mosquitto`  
>1437558553: Error: Invalid user 'mosquitto'.
```
    #增加mosquitto用户 
    groupadd mosquitto
    useradd -g mosuqitto mosquiotto
    或
    adduser mosquitto 
```

## 网上资料 
  1. [[原] mosquitto 使用时出现的一些问题及其解决办法  ](http://houjixin.blog.163.com/blog/static/3562841020156142544694/)
  2. [MQTT学习笔记——MQTT协议体验 Mosquitto安装和使用 ](http://blog.csdn.net/xukai871105/article/details/39252653)
  3. [mosquitto-1.4.9在Linux上的编译](http://www.liufei.com/liufei/coding/mqtt/)













