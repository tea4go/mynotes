##MQTT介绍
MQTT是IBM开发的一个即时通讯协议。MQTT是面向M2M和物联网的连接协议，采用轻量级发布和订阅消息传输机制。Mosquitto是一款实现了 MQTT v3.1 协议的开源消息代理软件，提供轻量级的，支持发布/订阅的的消息推送模式，使设备对设备之间的短消息通信简单易用。

若初次接触MQTT协议，可先理解以下概念

* 【MQTT协议特点】——相比于RESTful架构的物联网系统，MQTT协议借助消息推送功能，可以`更好地实现远程控制`。
* 【MQTT协议角色】——在RESTful架构的物联网系统，包含两个角色客户端和服务器端，而在MQTT协议中包括`发布者，代理器（服务器）和订阅者`。
* 【MQTT协议消息】——MQTT中的消息可理解为发布者和订阅者交换的内容（负载），这些消息包含具体的内容，可以被订阅者使用。
* 【MQTT协议主题】——MQTT中的主题可理解为相同类型或相似类型的消息集合。

##**安装**  

* **下载源码**  
      wget http://mosquitto.org/files/source/mosquitto-1.4.14.tar.gz    
* **解压**  
      tar zxfv mosquitto-1.4.5.tar.gz
* **编译&安装**    
      make
      make install

##**简单测试**
一个完整的MQTT示例包括一个代理器，一个发布者和一个订阅者。测试分为以下几个步骤：
1. 启动服务mosquitto。
2. 订阅者通过mosquitto_sub订阅指定主题的消息。
3. 发布者通过mosquitto_pub发布指定主题的消息。
4. 代理服务器把该主题的消息推送到订阅者。

##**FAQ**  
  1. 编译找不到openssl/ssl.h  
         #安装openssl    
         apt-get install libssl-dev
  2. 编译过程找不到ares.h  
         sudo apt-get install libc-ares-dev  
  3. 编译过程找不到uuid/uuid.h  
         sudo apt-get install uuid-dev  
  4. 使用过程中找不到libmosquitto.so.1  
    >error while loading shared libraries: libmosquitto.so.1: cannot open shared object file: No such file or directory
      
         #修改libmosquitto.so位置    
         ln -s /usr/local/lib/libmosquitto.so.1 /usr/lib/libmosquitto.so.1    
         ldconfig  
  5. make: g++：命令未找到  
         安装g++编译器  
         sudo apt-get install g++  
  6. Invalid user 'mosquitto'.  
     >[root@localhost src]# ./mosquitto`  
     >1437558553: Error: Invalid user 'mosquitto'.
                       
         #增加mosquitto用户 
         adduser mosquitto 

##**网上资料**   
  1. [[原] mosquitto 使用时出现的一些问题及其解决办法  ](http://houjixin.blog.163.com/blog/static/3562841020156142544694/)
  2. [MQTT学习笔记——MQTT协议体验 Mosquitto安装和使用 ](http://blog.csdn.net/xukai871105/article/details/39252653)




















---

jhhjh